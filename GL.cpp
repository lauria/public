#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
using namespace std;

union val_u {
 int i;
 char *s;
};



void serialize(char *buffer, const char *format, const union val_u src[], int size)
{
    int i;
    char l_buffer[256];

    if (!strcmp(format, "isi"))
    {
        for (i= 0; i < size; i++)
        {
            sprintf(l_buffer, "%d ", src[i].i);
            strcat(buffer, l_buffer);
        }
    }
    else
    {
        for (i= 0; i < size; i++)
        {
            sprintf(l_buffer, "%s ", src[i].s);
            strcat(buffer, l_buffer);
        } 
    }
}


void deserialize(union val_u dst[], const char *format, const char *buffer)
{
    int i= 0;
    char *str, *l_buffer;
   
    l_buffer = strdupa(buffer);
    if (!strcmp(format, "isi"))
    {
        str = strtok(l_buffer, " ");
        while(str != NULL)
        {
            dst[i].i = atoi(str);
            str = strtok(NULL, " ");
            i++;
        }
    }
    else
    {
        str = strtok(l_buffer, " ");
        while(str != NULL)
        {
            dst[i].s = strdup(str);
            str = strtok(NULL, " ");
            i++;
        }
    }
}


main ()
{
union val_u input[3];
union val_u output[3];
char buffer[256]= {};
union val_u s_input[3];
union val_u s_output[3];
char* s_buffer= new char[256];

input[0].i = 3;
input[1].i = 9;
input[2].i = 5;

serialize(buffer, "isi", input, 3);
cout << input[0].i << input[1].i << input[2].i << " =" << buffer;

deserialize(output, "isi", buffer);
cout << output[0].i << output[1].i << output[2].i << std::endl;
// ------------------------------------

s_input[0].s = new char[30];
strcpy(s_input[0].s, "first");
s_input[1].s = new char[30];
strcpy(s_input[1].s, "second");
s_input[2].s = new char[30];
strcpy(s_input[2].s, "third");

serialize(s_buffer, "isc", s_input, 3);
cout << s_input[0].s << s_input[1].s << s_input[2].s << " =" << s_buffer;

deserialize(s_output, "isc", s_buffer);
cout << s_output[0].s << s_output[1].s << s_output[2].s << std::endl;
}
